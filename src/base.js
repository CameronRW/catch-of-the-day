import Rebase from 're-base';
import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyCM83ft0fIc2eGT8c50Cogoe4Z0huS5Sxs",
  authDomain: "catch-of-the-day-cameronrw.firebaseapp.com",
  databaseURL: "https://catch-of-the-day-cameronrw.firebaseio.com",
});

const base = Rebase.createClass(firebaseApp.database());

// Named export
export { firebaseApp };

// Default export
export default base;
